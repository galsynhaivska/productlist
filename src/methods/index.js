
const validator = (value, pattern) => {
    return pattern.test(value);
}

export {validator}