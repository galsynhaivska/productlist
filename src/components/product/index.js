import "./product.css"

export const Product = ({className, placeholder, change, value}) => {
    return (
            <input type="text" className={className} placeholder={placeholder} value={value} onChange={(event)=>change(event.target.value)}></input>
       
    )
}