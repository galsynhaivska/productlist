import React from "react";
import {Component} from "react";
import {Btn} from "../btn";
import {Product} from "../product";
import {List} from "../list";
import {validator} from "../../methods"


import "./main.css"

let list = [];
let newList = [];

class Main extends Component{
    state = {
        show : "hidden",
        product : "",
        ischange : 0,
        disabled : "",

    }
    clickAdd = () => {
        this.setState((state) =>{
            return {
                ...state,
                show : "",
                product : "",
                disabled : "disabled",
            }
        })
    }
    changeInput = (text) => {   
        this.setState((state) =>{
            return {
                ...state,
                product : text,
            }
        })
    }

    clickRemove = () => { 
    //    console.log(validator(this.state.product, /([A-я ]{2}\w+)/)); 
        if(validator(this.state.product, /([A-я ]{2}\w+)/)){
            list.push(this.state.product);
            this.setState((state) =>{
                return {
                    ...state,
                    show : "hidden",     
                    product : "",   
                    disabled : "",               
                }        
            })        
        }else{
            alert("Помилка в назві продукту!");
            this.setState((state) =>{
                return {
                    ...state,
                    show : "hidden",   
                    product : "",      
                }        
            })
        }    
    }

    changeItem = (index) =>{
        this.setState((state) =>{
            return {
                ...state, 
                disabled : "disabled",   
            }
        })
            const divs = document.querySelector(`#${list[index] + index }`);
                const inps = document.createElement("input");
                if(!divs.classList.contains("int")){
                inps.value = divs.innerHTML 
                divs.innerHTML = ""
                divs.append(inps);  
               
                inps.addEventListener("blur", () => {
                    divs.innerHTML = inps.value;       
                  
                });
                }
                this.setState((state) =>{
                    return {
                        ...state, 
                        disabled : "",
                    }
                })
            }    
       
    deleteItem = (index) =>{
    //    console.log("change +", index);
        list.splice(index,1)
        newList.splice(index, 1)
        this.setState((state) =>{
            return {
                ...state,    
            }
        })
    }

    didItem = (index) =>{
    //    console.log("change +", index);
    const divs = document.querySelector(`#${list[index] + index }`);
     // console.log(divs);

        if(divs.classList.contains("int")){
            newList[index] = ""
        }else{
            newList[index] = "int" 
        } 
        
        this.setState((state) =>{
            return {
                ...state,          
            }
        })
    
    }    
               
    render() {        
        return (
            <>  
                <div className="main-container"> 
                    <Btn eventClick={this.clickAdd} className={"add " + this.state.disabled} value="Додати ➕"></Btn>    
                    <div className={"input-form-container " + this.state.show}>
                        <div className="input-form">
                            <label >Введіть продукт: </label>
                            <Product className="change-input" placeholder="Введіть продукт" value={this.state.product} change={this.changeInput}></Product>
                        </div>
                        <Btn  eventClick={this.clickRemove} className="save" value="Зберегти 📀"></Btn>  
                    </div>
                    <div className="line"></div>
                    <div className="list-container">
                        <h1 className="h1-list">Список продуктів</h1>
                        <div className="table">   
                            <div className="header-row">
                                <List className="list-cell-num" value="N"></List>
                                <List className="list-cell-item" value="Назва продукту"></List>
                                <List className="list-cell" value="Редагувати "></List>
                                <List className="list-cell" value="Видалити"></List>
                                <List className="list-cell" value="Куплено"></List>
                            </div>     
                            {list.map((el, index) => {
                                return (  
                                    <div className="list-row" key={index+1}>
                                        <List className={"list-cell-num " + newList[index]} value={index+1}></List>
                                      
                                        <div className={"list-cell-item " + newList[index]} id={el+index}>{el}</div>
                                        <button onClick={()=>this.changeItem(index)} className={"change-item " + this.state.disabled}>✍️</button>  
                                        <button onClick={()=>this.deleteItem(index)} className={"change-item " + this.state.disabled}>❌</button>      
                                        <button onClick={()=>this.didItem(index)} className={"change-item " + this.state.disabled}>✅</button>  
                                    </div>     
                                )  
                            })}        
                        </div>          
                    </div>    
                </div>
            </>
        )
    }
}
export default Main;