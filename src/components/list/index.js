import "./list.css";

export const List = ({value, className, id}) => {
    return (
        <div className={className} id={id}>{value}</div>
    )
}