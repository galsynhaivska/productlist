
export const Btn = ({value, className, eventClick, inputText}) => {
    return (
        
            <button type="button" className={className} onClick={()=>{eventClick(inputText)}}>{value}</button>    
    )
}